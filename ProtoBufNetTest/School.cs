﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtoBufNetTest
{
    [ProtoContract]
    class School
    {
        [ProtoMember(1)]
        public List<Student> Students { get; set; }
    }
}
