﻿using ProtoBuf;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ProtoBufNetTest
{
    class Program
    {
        static void Main(string[] args)
        {

            //System.IO.File.WriteAllText(string.Format(@"..\..\GeneratedCode\{0}.cs","User"), Serializer.GetProto<User>());

            GenerateProtoFiles();

        }

        private static void GenerateProtoFiles()
        {

           
            System.IO.File.WriteAllText(
                  string.Format(@"..\..\GeneratedCode\{0}.proto", "School"),
                  Serializer.GetProto<School>()
                  );
          
        }

    }

}
