﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProtoBufNetTest
{
    [ProtoContract]
    [ProtoInclude(100, typeof(StudentWithLogin))]
    public class Student
    {
        [ProtoMember(1)]
        public int Id {get;set;}
        [ProtoMember(2)]
        public string Name { get; set; }
        [ProtoMember(3)]
        public Address Address {get;set;}
    }
}
